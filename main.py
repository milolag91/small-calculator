import re

previous = 0
run = True
print("Type 'quit' to exit\n")

def performMath():
    global run
    global previous
    equation = input("Enter equation:")
    if equation == 'quit':
        run = False
    else:
        equation = re.sub('[a-zA-Z,.:()" "]', '', equation)
        previous = eval(equation)
        print(previous)

while run:
    performMath()